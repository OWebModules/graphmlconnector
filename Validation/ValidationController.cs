﻿using GraphMLConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateExportTypedTagsToGraphMLRequest(ExportTypedTagsToGraphMLRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateExportTypedTagsToGraphMLModel(ExportTypedTagsToGraphMLModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.RootConfig))
            {
                if (model.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "RootConfig is empty!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigsToPaths))
            {
                if (model.Configs.Count != model.ConfigsToPaths.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There are {model.Configs.Count} configs, but {model.ConfigsToPaths.Count} paths!";
                    return result;
                }

                try
                {
                    foreach (var pathRel in model.ConfigsToPaths)
                    {
                        if (System.IO.File.Exists(pathRel.Name_Other))
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = $"{pathRel.Name_Other} is existing!";
                            return result;
                        }
                        System.IO.File.WriteAllText(pathRel.Name_Other, "test");
                        System.IO.File.Delete(pathRel.Name_Other);
                    }
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;

                    
                }
                
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigsToTaggingSources))
            {
                if (model.Configs.Count != model.ConfigsToTaggingSources.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There are {model.Configs.Count} configs, but {model.ConfigsToTaggingSources.Count} tagging-sources!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateExportERModelRequest(ExportERModelRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }
            else if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetExportERModelModel(ExportERModelModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ExportERModelModel.ConfigItem))
            {
                var objectCount = dbReader.Objects1.Count;
                if (objectCount != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need 1 Config-Object, but you provided {objectCount} obejcts!";
                    return result;
                }

                model.ConfigItem = dbReader.Objects1.First();

                if (model.ConfigItem.GUID_Parent != ERModel.Config.LocalData.Class_Export_ER_Model_to_GraphML.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Config is not of class {ERModel.Config.LocalData.Class_Export_ER_Model_to_GraphML.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ExportERModelModel.ConfigItemToERModels))
            {
                if (!dbReader.ObjectRels.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No ER-Models found!";
                    return result;
                }

                model.ConfigItemToERModels = dbReader.ObjectRels;

                if (!model.ConfigItemToERModels.All(erMod => erMod.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_uses_ER_Model.ID_Class_Right))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not Only ER-Models found!";
                    return result;
                }
            }
            else if (propertyName == nameof(ExportERModelModel.ConfigItemToExportPath))
            {
                var pathsCount = dbReader.ObjectRels.Count;
                if (pathsCount != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one Path, but there are {pathsCount} Paths!";
                    return result;
                }

                model.ConfigItemToExportPath = dbReader.ObjectRels.First();
                if (model.ConfigItemToExportPath.ID_Parent_Other != ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_export_to_Path.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not of Class {ERModel.Config.LocalData.Class_Path.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ExportERModelModel.ConfigItemToExportType))
            {
                var exportTypesCount = dbReader.ObjectRels.Count;
                if (exportTypesCount > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need max one Export-Type, but there are {exportTypesCount} Export-Types!";
                    return result;
                }

                model.ConfigItemToExportType = dbReader.ObjectRels.FirstOrDefault();

                if (model.ConfigItemToExportType != null && model.ConfigItemToExportType.ID_Parent_Other != ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_is_of_Type_Export_Type__ER_Model_.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not of Class {ERModel.Config.LocalData.Class_Export_Type__ER_Model_.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ExportERModelModel.ERModelToEntities))
            {
                model.ERModelToEntities = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ExportERModelModel.ERModelToEntityRelations))
            {
                foreach (var erModel in model.ConfigItemToERModels)
                {
                    var erRelationsCount = dbReader.ObjectRels.Count(erModelToEntityRelation => erModelToEntityRelation.ID_Object == erModel.ID_Other);
                    if (erRelationsCount == 0)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"The model {erModel.Name_Other} as no relations!";
                        return result;
                    }
                }

                model.ERModelToEntityRelations = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ExportERModelModel.EntityRelationsToAttributes))
            {
                model.EntityRelationsToAttributes = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == ERModel.Config.LocalData.Class_Attribute__Entity_.GUID).ToList();
                model.EntityRelationsToCardinalitiesLeft = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_Left_Cardinality.ID_RelationType &&
                                                                                            rel.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Entity_Relation_Left_Cardinality.ID_Class_Right).ToList();

                model.EntityRelationsToCardinalitiesRight = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_Right_Cardinality.ID_RelationType &&
                                                                                            rel.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Entity_Relation_Right_Cardinality.ID_Class_Right).ToList();

                model.EntityRelationsToEntitiesLeft = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_Left_Entity.ID_RelationType &&
                                                                                            rel.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Entity_Relation_Left_Entity.ID_Class_Right).ToList();

                model.EntityRelationsToEntitiesRight = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_Right_Entity.ID_RelationType &&
                                                                                            rel.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Entity_Relation_Right_Entity.ID_Class_Right).ToList();

                model.EntityRelationsToModellingPatterns = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == ERModel.Config.LocalData.ClassRel_Entity_Relation_is_of_Type_Modelling_Pattern.ID_Class_Right).ToList();

                model.EntityRelations = (from erModelToEntityRelation in model.ERModelToEntityRelations
                                         join attributeConnected in model.EntityRelationsToAttributes.Where(att => att.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_connected_by_Attribute__Entity_.ID_RelationType).ToList() on erModelToEntityRelation.ID_Other equals attributeConnected.ID_Object
                                         join cardinalityLeft in model.EntityRelationsToCardinalitiesLeft on erModelToEntityRelation.ID_Other equals cardinalityLeft.ID_Object
                                         join cardinalityRight in model.EntityRelationsToCardinalitiesRight on erModelToEntityRelation.ID_Other equals cardinalityRight.ID_Object
                                         join entityLeft in model.EntityRelationsToEntitiesLeft on erModelToEntityRelation.ID_Other equals entityLeft.ID_Object
                                         join entityRight in model.EntityRelationsToEntitiesRight on erModelToEntityRelation.ID_Other equals entityRight.ID_Object
                                         join modellingPattern in model.EntityRelationsToModellingPatterns on erModelToEntityRelation.ID_Other equals modellingPattern.ID_Object into modellingPatterns
                                         from modellingPattern in modellingPatterns.DefaultIfEmpty()
                                         select new EntityRelation
                                         {
                                             IdErModel = erModelToEntityRelation.ID_Object,
                                             NameErModel = erModelToEntityRelation.Name_Object,
                                             IdEntityRelation = erModelToEntityRelation.ID_Other,
                                             NameEntityRelation = erModelToEntityRelation.Name_Other,
                                             IdAttributeConnected = attributeConnected.ID_Other,
                                             NameAttributeConnected = attributeConnected.Name_Other,
                                             IdCardinalityLeft = cardinalityLeft.ID_Other,
                                             NameCardinalityLeft = cardinalityLeft.Name_Other,
                                             IdCardinalityRight = cardinalityRight.ID_Other,
                                             NameCardinalityRight = cardinalityRight.Name_Other,
                                             IdEntityLeft = entityLeft.ID_Other,
                                             NameEntityLeft = entityLeft.Name_Other,
                                             IdEntityRight = entityRight.ID_Other,
                                             NameEntityRight = entityRight.Name_Other,
                                             IdModellingPattern = modellingPattern?.ID_Other,
                                             NameModellingPattern = modellingPattern?.Name_Other,
                                             RelAttributeDescribe = model.EntityRelationsToAttributes.Where(att => att.ID_Object == erModelToEntityRelation.ID_Other && att.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Relation_describes_Attribute__Entity_.ID_RelationType).ToList()
                                         }).ToList();

                
                var entityReltionCount = model.ERModelToEntityRelations.GroupBy(modRel => modRel.ID_Other).Count();
                if (model.EntityRelations.Count != entityReltionCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {entityReltionCount} Entity-Relations, but {model.EntityRelations.Count} related Entity-Relations!";
                    return result;
                }
            }
            else if (propertyName == nameof(ExportERModelModel.EntitiesToAttributesIdentify))
            {
                model.EntitiesToAttributesIdentify = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_identified_by_Attribute__Entity_.ID_RelationType).ToList();
                model.EntitiesToAttributesDescribe = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_contains_Attribute__Entity_.ID_RelationType).ToList();
                model.EntitiesToAttributesDerive = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ERModel.Config.LocalData.ClassRel_Entity_Derived_Attribute__Entity_.ID_RelationType).ToList();

                model.EntityAttributeIds = model.EntitiesToAttributesIdentify.GroupBy(att => att.ID_Other).Select(att => att.Key).ToList();
                model.EntityAttributeIds.AddRange(from attributeIdNew in model.EntitiesToAttributesDescribe.GroupBy(att => att.ID_Other).Select(att => att.Key).ToList()
                                      join attributeId in model.EntityAttributeIds on attributeIdNew equals attributeId into attributeIdsExisting
                                      from attributeId in attributeIdsExisting.DefaultIfEmpty()
                                      where attributeId == null
                                      select attributeIdNew);
                model.EntityAttributeIds.AddRange(from attributeIdNew in model.EntitiesToAttributesDerive.GroupBy(att => att.ID_Other).Select(att => att.Key).ToList()
                                      join attributeId in model.EntityAttributeIds on attributeIdNew equals attributeId into attributeIdsExisting
                                      from attributeId in attributeIdsExisting.DefaultIfEmpty()
                                      where attributeId == null
                                      select attributeIdNew);
            }
            else if (propertyName == nameof(ExportERModelModel.EntityAttributesIsEntityAttributes))
            {
                model.EntityAttributesIsEntityAttributes = (from rel in dbReader.ObjectRels
                                                            join attributeIdLeft in model.EntityAttributeIds on rel.ID_Object equals attributeIdLeft
                                                            join attributeIdRight in model.EntityAttributeIds on rel.ID_Other equals attributeIdRight
                                                            select rel).ToList();
            }

            return result;
        }
    }
}
