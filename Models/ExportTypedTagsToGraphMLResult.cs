﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class ExportTypedTagsToGraphMLResult
    {
        public string FilePath { get; set; }
    }
}
