﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class ExportERModelModel
    {
        public clsOntologyItem ConfigItem { get; set; }
        public List<clsObjectRel> ConfigItemToERModels { get; set; }
        public clsObjectRel ConfigItemToExportPath { get; set; }

        public clsObjectRel ConfigItemToExportType { get; set; }

        public List<clsObjectRel> ERModelToEntities { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ERModelToEntityRelations { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToAttributes { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToCardinalitiesLeft { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToCardinalitiesRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToEntitiesLeft { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToEntitiesRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntityRelationsToModellingPatterns { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> EntityAttributesIsEntityAttributes { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> EntitiesToAttributesIdentify { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EntitiesToAttributesDescribe { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> EntitiesToAttributesDerive { get; set; } = new List<clsObjectRel>();

        public List<EntityRelation> EntityRelations { get; set; } = new List<EntityRelation>();

        public List<string> EntityAttributeIds { get; set; } = new List<string>();
    }

    public class EntityRelation
    {
        public string IdErModel { get; set; }
        public string NameErModel { get; set; }
        public string IdEntityRelation { get; set; }
        public string NameEntityRelation { get; set; }

        public string IdAttributeConnected { get; set; }
        public string NameAttributeConnected { get; set; }

        public string IdCardinalityLeft { get; set; }
        public string NameCardinalityLeft { get; set; }

        public string IdCardinalityRight { get; set; }
        public string NameCardinalityRight { get; set; }

        public string IdEntityLeft { get; set; }
        public string NameEntityLeft { get; set; }

        public string IdEntityRight { get; set; }
        public string NameEntityRight { get; set; }

        public string IdModellingPattern { get; set; }
        public string NameModellingPattern { get; set; }

        public List<clsObjectRel> RelAttributeDescribe { get; set; } = new List<clsObjectRel>();
    }

}
