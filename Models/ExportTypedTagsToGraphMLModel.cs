﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class ExportTypedTagsToGraphMLModel
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToPaths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToTaggingSources { get; set; } = new List<clsObjectRel>();
    }
}
