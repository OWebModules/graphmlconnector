﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class ExportTypedTagsToGraphMLRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public ExportTypedTagsToGraphMLRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
