﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class NodeEdgePackage
    {
        public List<GroupItem> GroupItems { get; set; } = new List<GroupItem>();
        public List<NodeItem> NodeItems { get; set; } = new List<NodeItem>();
        public List<EdgeItem> EdgeItems { get; set; } = new List<EdgeItem>();
    }
}
