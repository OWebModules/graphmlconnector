﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class SubNodeList
    {
        public string Variable { get; set; }
        public List<NodeItem> SubNodes { get; set; } = new List<NodeItem>();        
    }
}
