﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public enum GraphType
    {
        RDF = 0,
        OntoObject = 1,
        OntoClasses = 2
    }
    public class ExportOntologyToMLGraphRequest
    {
        public string IdOntology { get; set; }
        public string GraphMLFileName { get; set; }
        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public GraphType GraphType { get; private set; }
        public string UrlTemplate { get; set; }

        public ExportOntologyToMLGraphRequest(string idOntology, string graphMLFileName, GraphType graphType)
        {
            IdOntology = idOntology;
            GraphMLFileName = graphMLFileName;
            GraphType = graphType;
        }
    }
}
