﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class NodeItem
    {
        public string IdNode { get; set; }
        public string NameNode { get; set; }
        public int SVGSourceId { get; set; }

        public string FillColor { get; set; } = "#00ff00";
        public string TextColor { get; set; } = "#ffffff";

        public string ShapeType { get; set; } = "";
        public clsOntologyItem XmlTemplate { get; set; }
        public List<SubNodeList> SubNodeList { get; set; } = new List<SubNodeList>();
        public List<VariableValueMap> VariableValueMapList { get; set; } = new List<VariableValueMap>();
    }
}
