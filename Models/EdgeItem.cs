﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    
    public enum ArrowType
    {
        none = 0,
        standard = 1
    }
    public class EdgeItem
    {
        public NodeItem NodeItem1 { get; set; }
        public NodeItem NodeItem2 { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public int SVGSourceId { get; set; }
        public clsOntologyItem XmlTemplate { get; set; }

        public ArrowType ArrowTypeSource { get; set; }
        public ArrowType ArrowTypeTarget { get; set; }

        public List<VariableValueMap> VariableValueMapList { get; set; } = new List<VariableValueMap>();

    }
}
