﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class ExportGraphRequest
    {
        public string FileName { get; set; }
        public List<NodeItem> Nodes { get; set; } = new List<NodeItem>();
        public List<EdgeItem> Edges { get; set; } = new List<EdgeItem>();
        public List<GroupItem> GroupItems { get; set; } = new List<GroupItem>();

        public List<clsOntologyItem> SvgSources { get; set; } = new List<clsOntologyItem>();

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }


    }
}
