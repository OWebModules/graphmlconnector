﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class GroupItem
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string GraphId { get; set; }

        public List<NodeItem> Nodes { get; set; } = new List<NodeItem>();
        public List<EdgeItem> Edges { get; set; } = new List<EdgeItem>();

        public GroupItem ParentGroupItem { get; set; }
        public List<GroupItem> GroupItems { get; set; } = new List<GroupItem>();

        public List<GroupItem> GetAllSubGroupItems()
        {
            var result = GroupItems.ToList();

            var subGroupItems = new List<GroupItem>();
            foreach (var groupItem in result)
            {
                subGroupItems.AddRange(groupItem.GetAllSubGroupItems());
            }

            result.AddRange(subGroupItems);

            return result;
        }
    }
}
