﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Models
{
    public class VariableValueMap
    {
        public string Variable { get; set; }
        public string Value { get; set; }
    }
}
