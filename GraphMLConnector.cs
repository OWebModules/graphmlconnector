﻿using GraphMLConnector.Factories;
using GraphMLConnector.Models;
using GraphMLConnector.Services;
using GraphMLConnector.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TypedTaggingModule.Connectors;

namespace GraphMLConnector
{

    public enum ExportType
    {
        All = 0,
        Entities = 1,
        EntitiesWithIdentAtt = 2,
        EntitiesWithIdentDerivedAtt = 3,
        EntitiesWithDerivedAtt = 4
    }
    public class GraphMLConnector : AppController
    {
        public async Task<ResultItem<ExportTypedTagsToGraphMLResult>> ExportTypedTagsToGraphML(ExportTypedTagsToGraphMLRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExportTypedTagsToGraphMLResult>>(async () =>
           {
               var result = new ResultItem<ExportTypedTagsToGraphMLResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ExportTypedTagsToGraphMLResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateExportTypedTagsToGraphMLRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {

                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");


               var elasticAgent = new ElasticServiceAgent(Globals);

               request.MessageOutput?.OutputInfo($"Get config: {request.IdConfig}");

               var modelResult = await elasticAgent.GetExportTypedTagsToGraphMLModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }


               var graphMLTemplatesFactory = new GraphMLUMLTemplatesFactory(Globals);

               graphMLTemplatesFactory.Initialize();

               foreach (var config in modelResult.Result.Configs)
               {
                   var taggingSource = modelResult.Result.ConfigsToTaggingSources.FirstOrDefault(tagSource => tagSource.ID_Object == config.GUID);
                   var path = modelResult.Result.ConfigsToPaths.FirstOrDefault(pathItm => pathItm.ID_Object == config.GUID);


                   request.MessageOutput?.OutputInfo($"Have tagging-source: {taggingSource.Name_Other}");

                   request.MessageOutput?.OutputInfo("Get Typed Tags...");
                   var typedTaggingController = new TypedTaggingConnector(Globals);
                   var typedTags = await typedTaggingController.GetTags(taggingSource.ID_Other, string.Empty, string.Empty);

                   result.ResultState = typedTags.Result;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Typed Tags!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.MessageOutput?.OutputInfo($"Have {typedTags.Result.Count} Typed Tags.");

                   request.MessageOutput?.OutputInfo($"Get list of classes to search...");

                   var classIds = typedTags.References.SelectMany(typedTag => typedTag.Tags).GroupBy(tag => tag.IdTagParent).Select(classId => new clsOntologyItem
                   {
                       GUID = classId.Key
                   }).ToList();

                   var countToSearch = classIds.Count;
                   request.MessageOutput?.OutputInfo($"Search {countToSearch} classes...");

                   var classResult = await elasticAgent.GetClasses(classIds);

                   result.ResultState = classResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   }

                   request.MessageOutput?.OutputInfo($"Have {classResult.Result.Count} classes!");

                   if (countToSearch != classResult.Result.Count)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = $"Found not all classes: searched {countToSearch}, found {classResult.Result.Count}";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var objects = typedTags.References.SelectMany(typedTag => typedTag.Tags).Select(tag => new clsOntologyItem
                   {
                       GUID = tag.IdTag,
                       Name = tag.NameTag,
                       GUID_Parent = tag.IdTagParent,
                       Type = tag.TagType
                   }).ToList();

                   using (var textWriter = new StreamWriter(path.Name_Other))
                   {
                       graphMLTemplatesFactory.WritePreContainer((text) =>
                       {
                           textWriter.WriteLine(text);
                       }, Config.LocalData.Object_Graphml___Container);


                       var taggingSourceNode = new NodeItem
                       {
                           IdNode = taggingSource.ID_Other,
                           NameNode = taggingSource.Name_Other,
                           XmlTemplate = Config.LocalData.Object_Graphml___Object_Node,
                           ShapeType = Config.LocalData.Object_rectangle.Name,
                           FillColor = "#ccffff",
                           TextColor = "#000000"
                       };

                       graphMLTemplatesFactory.WriteNodeXML(taggingSourceNode, (text) =>
                       {
                           textWriter.WriteLine(text);
                       });

                       foreach (var clsItm in classResult.Result)
                       {
                           var classNode = new NodeItem
                           {
                               IdNode = clsItm.GUID,
                               NameNode = clsItm.Name,
                               XmlTemplate = Config.LocalData.Object_Graphml___RDF,
                               ShapeType = Config.LocalData.Object_ellipse.Name,
                               FillColor = "#ffff00",
                               TextColor = "#000000"
                           };

                           graphMLTemplatesFactory.WriteNodeXML(classNode, (text) =>
                           {
                               textWriter.WriteLine(text);
                           });

                           var edgeItem = new EdgeItem
                           {
                               NodeItem1 = taggingSourceNode,
                               NodeItem2 = classNode
                           };

                           graphMLTemplatesFactory.WriteEdgeXML(edgeItem, (text) =>
                           {
                               textWriter.WriteLine(text);
                           });

                           var objectNodes = objects.Where(obj => obj.GUID_Parent == classNode.IdNode).Select(obj => new NodeItem
                           {
                               IdNode = obj.GUID,
                               NameNode = obj.Name,
                               XmlTemplate = Config.LocalData.Object_Graphml___Object_Node,
                               ShapeType = Config.LocalData.Object_rectangle.Name,
                               FillColor = "#ccffff",
                               TextColor = "#000000"
                           });

                           foreach (var objectNode in objectNodes)
                           {
                               graphMLTemplatesFactory.WriteNodeXML(objectNode, (text) =>
                               {
                                   textWriter.WriteLine(text);
                               });

                               edgeItem = new EdgeItem
                               {
                                   NodeItem1 = classNode,
                                   NodeItem2 = objectNode
                               };

                               graphMLTemplatesFactory.WriteEdgeXML(edgeItem, (text) =>
                               {
                                   textWriter.WriteLine(text);
                               });


                           }
                       }


                       graphMLTemplatesFactory.WritePostContainer(new List<clsOntologyItem>(), (text) =>
                       {
                           textWriter.WriteLine(text);
                       }, Config.LocalData.Object_Graphml___Container);
                   }
               }







               return result;
           });

            return taskResult;
        }
        public async Task<clsOntologyItem> ExportOntologyToMLGraph(ExportOntologyToMLGraphRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var dbReader = new OntologyModDBConnector(Globals);

                var resultOItem = dbReader.GetOItem(request.IdOntology, Globals.Type_Object);
                if (resultOItem.GUID_Related == Globals.LState_Error.GUID)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "Error while getting the Ontology-Item!";
                    return result;
                }

                if (resultOItem.GUID_Parent != Globals.Class_Ontologies.GUID)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "The requested item is no Ontology!";
                    return result;
                }

                var ontologyConnector = new OntologyConnector(Globals);

                var ontologyRequest = new GetOntologyRequest(resultOItem);
                var ontologyResult = await ontologyConnector.GetOntologies(ontologyRequest);

                result = ontologyResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var enrichtResult = await ontologyConnector.EnrichOntologies(ontologyResult.Result, false);

                result = enrichtResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                GraphMLTemplatesFactory templatesFactory = null;
                clsOntologyItem containerTemplate = null;
                if (request.GraphType == GraphType.RDF)
                {
                    templatesFactory = new GraphMLUMLTemplatesFactory(Globals);
                    containerTemplate = Config.LocalData.Object_Graphml___Container;
                }
                else 
                {
                    templatesFactory = new GraphMLTemplatesFactory(Globals);
                    containerTemplate = Config.LocalData.Object_GraphML___Onto___Container;
                }
                
                request.MessageOutput?.OutputInfo("Initialize UML-Templates...");
                result = templatesFactory.Initialize();

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Initialized UML-Templates.");

                try
                {
                    using (TextWriter textWriter = new StreamWriter(request.GraphMLFileName))
                    {
                        request.MessageOutput?.OutputInfo("Write Container init...");
                        var writePreContainerResult = templatesFactory.WritePreContainer((text) =>
                        {
                            textWriter.WriteLine(text);
                        }, containerTemplate);

                        result = writePreContainerResult;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        request.MessageOutput?.OutputInfo("Written Container init.");

                        var nodeEdgePackage = new NodeEdgePackage();

                        var classes = enrichtResult.Result.SelectMany(res => res.Classes).GroupBy(grp => new { grp.GUID, grp.Name, grp.GUID_Parent })
                            .Select(grp => new clsOntologyItem { GUID = grp.Key.GUID, Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent, Type = Globals.Type_Class }).ToList();
                        var attributeTypes = enrichtResult.Result.SelectMany(res => res.AttributeTypes).GroupBy(grp => new { grp.GUID, grp.Name, grp.GUID_Parent })
                            .Select(grp => new clsOntologyItem { GUID = grp.Key.GUID, Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent } ).ToList();
                        var relationTypes = enrichtResult.Result.SelectMany(res => res.RelationTypes).GroupBy(grp => new { grp.GUID, grp.Name })
                            .Select(grp => new clsOntologyItem { GUID = grp.Key.GUID, Name = grp.Key.Name, Type = Globals.Type_RelationType }).ToList() ;
                        var objects = enrichtResult.Result.SelectMany(res => res.Objects).GroupBy(grp => new { grp.GUID, grp.Name, grp.GUID_Parent })
                            .Select(grp => new clsOntologyItem { GUID = grp.Key.GUID, Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent, Type = Globals.Type_Object}).ToList();
                        var objAtts = enrichtResult.Result.SelectMany(res => res.ObjectAttributes).ToList();
                        var objRels = enrichtResult.Result.SelectMany(res => res.ObjectRelations).ToList();
                        var classAtts = enrichtResult.Result.SelectMany(res => res.ClassAttributes).ToList();
                        var classRels = enrichtResult.Result.SelectMany(res => res.ClassRelations).ToList();

                        if (request.GraphType == GraphType.RDF)
                        {
                            result = ExportOntologyToRDFGraph(request,
                                templatesFactory,
                                textWriter,
                                classes,
                                objects,
                                attributeTypes,
                                relationTypes,
                                enrichtResult.Result);
                        }
                        else if (request.GraphType == GraphType.OntoObject)
                        {
                            result = ExportOntologyToObjectGraph(request,
                                templatesFactory,
                                textWriter,
                                classes,
                                objects,
                                attributeTypes,
                                relationTypes,
                                objAtts,
                                objRels,
                                enrichtResult.Result);
                        }
                        else if (request.GraphType == GraphType.OntoClasses)
                        {
                            result = ExportOntologyToClassGraph(request,
                                templatesFactory,
                                textWriter,
                                classes,
                                attributeTypes,
                                relationTypes,
                                classAtts,
                                classRels,
                                enrichtResult.Result);
                        }
                        

                        var writeContainerRestResult = templatesFactory.WritePostContainer(new List<clsOntologyItem>(), (text) =>
                        {
                            textWriter.WriteLine(text);
                        }, containerTemplate);

                        result = writeContainerRestResult;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                    }
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        private clsOntologyItem ExportOntologyToRDFGraph(ExportOntologyToMLGraphRequest request, 
            GraphMLTemplatesFactory factory,
            TextWriter textWriter,
            List<clsOntologyItem> classes,
            List<clsOntologyItem> objects,
            List<clsOntologyItem> attributeTypes,
            List<clsOntologyItem> relationTypes,
            List<Ontology> ontologies)
        {
            var result = Globals.LState_Success.Clone();
            var classNodes = classes.Select(cls => new NodeItem
            {
                IdNode = cls.GUID,
                NameNode = string.Format(request.UrlTemplate, HttpUtility.UrlEncode(cls.Name)),
                XmlTemplate = Config.LocalData.Object_Graphml___RDF,
                ShapeType = Config.LocalData.Object_ellipse.Name,
                FillColor = "#ffffff",
                TextColor = "#000000"
            }).ToList();

            var classLabels = classes.Select(cls => new NodeItem
            {
                IdNode = $"label_{cls.GUID}",
                NameNode = $"\"{cls.Name}\"",
                XmlTemplate = Config.LocalData.Object_Graphml___RDF,
                ShapeType = Config.LocalData.Object_rectangle.Name,
                FillColor = "#ffffff",
                TextColor = "#000000"
            }).ToList();

            foreach (var nodeItem in classNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            foreach (var nodeItem in classLabels)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var attributeTypeNodes = attributeTypes.Select(attType => new NodeItem
            {
                IdNode = attType.GUID,
                NameNode = string.Format(request.UrlTemplate, HttpUtility.UrlEncode(attType.Name)),
            }).ToList();

            foreach (var nodeItem in attributeTypeNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var objectNodes = objects.Select(obj => new NodeItem
            {
                IdNode = obj.GUID,
                NameNode = string.Format(request.UrlTemplate, HttpUtility.UrlEncode(obj.Name)),
                XmlTemplate = Config.LocalData.Object_Graphml___RDF,
                ShapeType = Config.LocalData.Object_ellipse.Name,
                FillColor = "#ffffff",
                TextColor = "#000000"
            }).ToList();

            foreach (var nodeItem in objectNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var dummyRelType = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = ""
            };
            var edgesClassAttributes = (from classAttr in ontologies.SelectMany(res => res.ClassAttributes)
                                        join classItem in classNodes on classAttr.ID_Class equals classItem.IdNode
                                        join attributeType in attributeTypeNodes on classAttr.ID_AttributeType equals attributeType.IdNode
                                        select new EdgeItem
                                        {
                                            NodeItem1 = classItem,
                                            NodeItem2 = attributeType,
                                            RelationType = dummyRelType
                                        }).ToList();

            foreach (var edge in edgesClassAttributes)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var edgesClasRelations = (from classRel in ontologies.SelectMany(res => res.ClassRelations)
                                      join classItemLeft in classNodes on classRel.ID_Class_Left equals classItemLeft.IdNode
                                      join relationType in relationTypes on classRel.ID_RelationType equals relationType.GUID
                                      join classItemRight in classNodes on classRel.ID_Class_Right equals classItemRight.IdNode
                                      select new EdgeItem
                                      {
                                          NodeItem1 = classItemLeft,
                                          NodeItem2 = classItemRight,
                                          RelationType = new clsOntologyItem { GUID = relationType.GUID, Name = string.Format(request.UrlTemplate, HttpUtility.UrlEncode(relationType.Name)) }
                                      }).ToList();

            foreach (var edge in edgesClasRelations)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }
            var relClassLbl = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdf:label"
            };

            var edgesClassLabels = (from classNode in classNodes
                                    join labelNode in classLabels on $"label_{classNode.IdNode}" equals labelNode.IdNode
                                    select new EdgeItem
                                    {
                                        NodeItem1 = classNode,
                                        NodeItem2 = labelNode,
                                        RelationType = relClassLbl
                                    }).ToList();

            foreach (var edge in edgesClassLabels)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }


            var relClassObj = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdf:type"
            };

            var edgesClassObjects = (from objectNode in objectNodes
                                     join objectItem in objects on objectNode.IdNode equals objectItem.GUID
                                     join classNode in classNodes on objectItem.GUID_Parent equals classNode.IdNode
                                     select new EdgeItem
                                     {
                                         NodeItem1 = objectNode,
                                         NodeItem2 = classNode,
                                         RelationType = relClassObj
                                     }).ToList();

            foreach (var edge in edgesClassObjects)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            result = CreateClassHierarchiyEdges(textWriter, classNodes, classes, factory, request.MessageOutput);

            if (result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            return result;
        }

        private clsOntologyItem ExportOntologyToObjectGraph(ExportOntologyToMLGraphRequest request,
            GraphMLTemplatesFactory factory,
            TextWriter textWriter,
            List<clsOntologyItem> classes,
            List<clsOntologyItem> objects,
            List<clsOntologyItem> attributeTypes,
            List<clsOntologyItem> relationTypes,
            List<clsObjectAtt> objectAttributes,
            List<clsObjectRel> objectRelations,
            List<Ontology> ontologies)
        {
            var result = Globals.LState_Success.Clone();
            var classNodes = classes.Where(cls => objects.Any(obj => obj.GUID_Parent == cls.GUID)).Select(cls => new NodeItem
            {
                IdNode = cls.GUID,
                NameNode = cls.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
                ShapeType = Config.LocalData.Object_octagon.Name,
                FillColor = "#ffffff",
                TextColor = "#000000"
            }).ToList();

            foreach (var nodeItem in classNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var attributeTypeNodes = attributeTypes.Select(attType => new NodeItem
            {
                IdNode = attType.GUID,
                NameNode = attType.Name,
                FillColor = "",
                ShapeType = Config.LocalData.Object_octagon.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
            }).ToList();

            foreach (var nodeItem in attributeTypeNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var relationTypeNodes = relationTypes.Select(relType => new NodeItem
            {
                IdNode = relType.GUID,
                NameNode = relType.Name,
                FillColor = "#FFCC00",
                TextColor = "#000000",
                ShapeType = Config.LocalData.Object_diamond.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
            }).ToList();

            foreach (var nodeItem in relationTypeNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var objectNodes = objects.Select(obj => new NodeItem
            {
                IdNode = obj.GUID,
                NameNode = obj.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
                ShapeType = Config.LocalData.Object_roundrectangle.Name,
                FillColor = "#FFCC00",
                TextColor = "#000000"
            }).ToList();

            foreach (var nodeItem in objectNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var objectAttributeNotes = objectAttributes.Select(objAtt => new NodeItem
            {
                IdNode = objAtt.ID_Attribute,
                NameNode = objAtt.Val_Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
                ShapeType = Config.LocalData.Object_ellipse.Name,
                FillColor = "#CCFFFF",
                TextColor = "#00000"
            }).ToList();

            foreach (var nodeItem in objectAttributeNotes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var dummyRelType = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = ""
            };
            
            var relClassLbl = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdf:label"
            };

            var relClassObj = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdf:type"
            };

            var edgesClassObjects = (from objectNode in objectNodes
                                     join objectItem in objects on objectNode.IdNode equals objectItem.GUID
                                     join classNode in classNodes on objectItem.GUID_Parent equals classNode.IdNode
                                     select new EdgeItem
                                     {
                                         NodeItem1 = objectNode,
                                         NodeItem2 = classNode,
                                         RelationType = relClassObj,
                                         XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                     }).ToList();

            foreach (var edge in edgesClassObjects)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var edgeObjectAttributes = (from objectNode in objectNodes
                                        join objAtt in objectAttributes on objectNode.IdNode equals objAtt.ID_Object
                                        join objATtNode in objectAttributeNotes on objAtt.ID_AttributeType equals objATtNode.IdNode
                                        select new EdgeItem
                                        {
                                            NodeItem1 = objectNode,
                                            NodeItem2 = objATtNode,
                                            ArrowTypeTarget = ArrowType.standard,
                                            XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                        }).ToList();


            foreach (var edge in edgeObjectAttributes)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var edgeObjAttToAttType = (from objAtt in objectAttributes
                                   join objAttNode in objectAttributeNotes on objAtt.ID_Attribute equals objAttNode.IdNode
                                   join attTypeNode in attributeTypeNodes on objAtt.ID_AttributeType equals attTypeNode.IdNode
                                   select new EdgeItem
                                   {
                                       NodeItem1 = objAttNode,
                                       NodeItem2 = attTypeNode,
                                       XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                   }).ToList();

            foreach (var edge in edgeObjAttToAttType)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var edgeObjectRels = new List<EdgeItem>();
            (from objRel in objectRelations
             join relTypeNode in relationTypeNodes on objRel.ID_RelationType equals relTypeNode.IdNode
             join objectNodeLeft in objectNodes on objRel.ID_Object equals objectNodeLeft.IdNode
             join objectNodeRight in objectNodes on objRel.ID_Other equals objectNodeRight.IdNode
             select new { relTypeNode, objectNodeLeft, objectNodeRight }).ToList().ForEach(nodes =>
               {
                   edgeObjectRels.Add(new EdgeItem
                   {
                       NodeItem1 = nodes.objectNodeLeft,
                       NodeItem2 = nodes.relTypeNode,
                       XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                   });
                   edgeObjectRels.Add(new EdgeItem
                   {
                       NodeItem1 = nodes.relTypeNode,
                       NodeItem2 = nodes.objectNodeRight,
                       ArrowTypeTarget = ArrowType.standard,
                       XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                   });
               });

            foreach (var edge in edgeObjectRels)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            return result;
        }

        private clsOntologyItem ExportOntologyToClassGraph(ExportOntologyToMLGraphRequest request,
            GraphMLTemplatesFactory factory,
            TextWriter textWriter,
            List<clsOntologyItem> classes,
            List<clsOntologyItem> attributeTypes,
            List<clsOntologyItem> relationTypes,
            List<clsClassAtt> classAttributes,
            List<clsClassRel> classRelations,
            List<Ontology> ontologies)
        {
            var result = Globals.LState_Success.Clone();
            var classNodes = classes.Select(cls => new NodeItem
            {
                IdNode = cls.GUID,
                NameNode = cls.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
                ShapeType = Config.LocalData.Object_octagon.Name,
                FillColor = "#ffffff",
                TextColor = "#000000"
            }).ToList();

            foreach (var nodeItem in classNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            var attributeTypeNodes = attributeTypes.Select(attType => new NodeItem
            {
                IdNode = attType.GUID,
                NameNode = attType.Name,
                FillColor = "",
                ShapeType = Config.LocalData.Object_octagon.Name,
                XmlTemplate = Config.LocalData.Object_GraphML___Onto___Object,
            }).ToList();

            foreach (var nodeItem in attributeTypeNodes)
            {
                var prepareNodeXMLResult = factory.WriteNodeXML(nodeItem, (text) =>
                {
                    textWriter.WriteLine(text);
                });

                result = prepareNodeXMLResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var relClassAtt = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdf:type"
            };

            var edgesClassAtt = (from clsAtt in classAttributes
                                 join classNode in classNodes on clsAtt.ID_Class equals classNode.IdNode
                                 join attTypeNode in attributeTypeNodes on clsAtt.ID_AttributeType equals attTypeNode.IdNode
                                 select new EdgeItem
                                 {
                                     NodeItem1 = classNode,
                                     NodeItem2 = attTypeNode,
                                     ArrowTypeTarget = ArrowType.standard,
                                     XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                 }).ToList();

            foreach (var edge in edgesClassAtt)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var edgesClassRel = (from clsRel in classRelations
                                     join classNodeLeft in classNodes on clsRel.ID_Class_Left equals classNodeLeft.IdNode
                                     join classNodeRight in classNodes on clsRel.ID_Class_Right equals classNodeRight.IdNode
                                     join relationtypeNode in relationTypes on clsRel.ID_RelationType equals relationtypeNode.GUID
                                     select new EdgeItem
                                     {
                                         NodeItem1 = classNodeLeft,
                                         NodeItem2 = classNodeRight,
                                         ArrowTypeTarget = ArrowType.standard,
                                         XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                     }).ToList();

            foreach (var edge in edgesClassRel)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            var edgeClassHierarchy = (from cls in classes.Where(cls1 => string.IsNullOrEmpty(cls1.GUID_Parent)).ToList()
                                        join classNodeParent in classNodes on cls.GUID_Parent equals classNodeParent.IdNode
                                        join classNodeChild in classNodes on cls.GUID equals classNodeChild.IdNode
                                        select new EdgeItem
                                        {
                                            NodeItem1 = classNodeChild,
                                            NodeItem2 = classNodeParent,
                                            ArrowTypeTarget = ArrowType.standard,
                                            XmlTemplate = Config.LocalData.Object_GraphML___Onto___Relation
                                        }).ToList();


            foreach (var edge in edgeClassHierarchy)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            }

            return result;
        }

        private clsOntologyItem CreateClassHierarchiyEdges(TextWriter textWriter, List<NodeItem> classNodes, List<clsOntologyItem> classes, GraphMLTemplatesFactory factory, IMessageOutput messageOutput, NodeItem parentItem = null)
        {
            var relType = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = "rdfs:subClassOf"
            };

            var result = Globals.LState_Success.Clone();

            var edges = new List<EdgeItem>();
            if (parentItem == null)
            {
                var rootClasses = (from classNode in classNodes
                                   join cls in classes on classNode.IdNode equals cls.GUID
                                   where string.IsNullOrEmpty(cls.GUID_Parent)
                                   select classNode).ToList();

                if (!rootClasses.Any())
                {
                    rootClasses = (from classNode in classNodes
                                   join cls in classes on classNode.IdNode equals cls.GUID
                                   where cls.GUID_Parent == Globals.Root.GUID
                                   select classNode).ToList();
                }

                edges = (from rootClass in rootClasses
                         join cls in classes on rootClass.IdNode equals cls.GUID_Parent
                         join classNode in classNodes on cls.GUID equals classNode.IdNode
                         select new EdgeItem
                         {
                             NodeItem1 = classNode,
                             NodeItem2 = rootClass,
                             RelationType = relType
                         }).ToList();


            }
            else
            {
                edges = (from classItem in classes.Where(cls => cls.GUID_Parent == parentItem.IdNode)
                         join classNode in classNodes on classItem.GUID equals classNode.IdNode
                         select new EdgeItem
                         {
                             NodeItem1 = classNode,
                             NodeItem2 = parentItem,
                             RelationType = relType
                         }).ToList();
            }


            foreach (var edge in edges)
            {
                var prepareEdgeResult = factory.WriteEdgeXML(edge, (text) =>
                {
                    textWriter.WriteLine(text);
                });
                result = prepareEdgeResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

            }

            foreach (var nodeGrp in edges.GroupBy(edge => edge.NodeItem1))
            {
                result = CreateClassHierarchiyEdges(textWriter, classNodes, classes, factory, messageOutput, nodeGrp.Key);
            }

            return result;
        }


        public async Task<clsOntologyItem> ExportGraph(ExportGraphRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                var umlTemplates = new GraphMLUMLTemplatesFactory(Globals);

                request.MessageOutput?.OutputInfo("Initialize UML-Templates...");
                result = umlTemplates.Initialize();

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Initialized UML-Templates.");

                try
                {
                    using (TextWriter textWriter = new StreamWriter(request.FileName))
                    {
                        request.MessageOutput?.OutputInfo("Write Container init...");
                        var writePreContainerResult = umlTemplates.WritePreContainer((text) =>
                        {
                            textWriter.WriteLine(text);
                        }, Config.LocalData.Object_Graphml___Container);

                        result = writePreContainerResult;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        request.MessageOutput?.OutputInfo("Written Container init.");

                        var nodeEdgePackage = new NodeEdgePackage();
                        request.MessageOutput?.OutputInfo($"Create {request.GroupItems.Count} Groups...");
                        foreach (var groupItem in request.GroupItems)
                        {
                            var createGroupResult = umlTemplates.CreateGroup(groupItem, (text) =>
                            {
                                textWriter.WriteLine(text);
                            }, request.MessageOutput);

                            result = createGroupResult.ResultState;
                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.Additional1);
                                return result;
                            }

                            nodeEdgePackage = createGroupResult.Result;
                        }

                        request.MessageOutput?.OutputInfo($"Created {request.GroupItems.Count} Groups.");

                        var nodesRest = (from node in request.Nodes
                                         join nodeDone in nodeEdgePackage.NodeItems on node equals nodeDone into nodesDone1
                                         from nodeDone in nodesDone1.DefaultIfEmpty()
                                         where nodeDone == null
                                         select node);

                        foreach (var nodeItem in nodesRest)
                        {
                            var prepareNodeXMLResult = umlTemplates.WriteNodeXML(nodeItem, (text) =>
                            {
                                textWriter.WriteLine(text);
                            });

                            result = prepareNodeXMLResult;
                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.Additional1);
                                return result;
                            }

                        }

                        var edgesRest = (from edge in request.Edges
                                         join edgeDone in nodeEdgePackage.EdgeItems on edge equals edgeDone into edgesDone1
                                         from edgeDone in edgesDone1.DefaultIfEmpty()
                                         where edgeDone == null
                                         select edge);

                        foreach (var edge in edgesRest)
                        {
                            var prepareEdgeResult = umlTemplates.WriteEdgeXML(edge, (text) =>
                            {
                                textWriter.WriteLine(text);
                            });
                            result = prepareEdgeResult;
                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.Additional1);
                                return result;
                            }

                        }

                        var writeContainerRestResult = umlTemplates.WritePostContainer(request.SvgSources, (text) =>
                        {
                            textWriter.WriteLine(text);
                        }, Config.LocalData.Object_Graphml___Container);

                        result = writeContainerRestResult;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                    }
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<string>> ExportErModelToGraphML(ExportERModelRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<string>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateExportERModelRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get model...");

                var elasticAgent = new ElasticServiceAgent(Globals);

                var modelResult = await elasticAgent.GetExportERModelModel(request);

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                var exportType = ExportType.All;
                if (modelResult.Result.ConfigItemToExportType != null)
                {
                    if (modelResult.Result.ConfigItemToExportType.ID_Other == ERModel.Config.LocalData.Object_Entities_only.GUID)
                    {
                        exportType = ExportType.Entities;
                    }
                    else if (modelResult.Result.ConfigItemToExportType.ID_Other == ERModel.Config.LocalData.Object_Entities_with_Derived_Attributes.GUID)
                    {
                        exportType = ExportType.EntitiesWithDerivedAtt;
                    }
                    else if (modelResult.Result.ConfigItemToExportType.ID_Other == ERModel.Config.LocalData.Object_Entities_with_Identifying_Attributes.GUID)
                    {
                        exportType = ExportType.EntitiesWithIdentAtt;
                    }
                    else if (modelResult.Result.ConfigItemToExportType.ID_Other == ERModel.Config.LocalData.Object_Entities_with_Identifying_and_Derived_Attributes.GUID)
                    {
                        exportType = ExportType.EntitiesWithIdentDerivedAtt;
                    }
                }

                var factory = new GraphMLTemplatesFactory(Globals);
                var relations = new List<EdgeItem>();

                var entityNodes = modelResult.Result.EntityRelations.Select(er => new NodeItem
                {
                    IdNode = er.IdEntityLeft,
                    NameNode = er.NameEntityLeft,
                    FillColor = "#FFCC00",
                    ShapeType = "rectangle",
                    TextColor = "#000000",
                    XmlTemplate = ERModel.Config.LocalData.Object_Graphml___ER_Node,
                    VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_ID.Name,
                            Value = er.IdEntityLeft
                        },
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_NAME_NODE.Name,
                            Value = er.NameEntityLeft
                        }
                    }
                }).ToList();

                entityNodes.AddRange(modelResult.Result.ERModelToEntities.Select(er => new NodeItem
                {
                    IdNode = er.ID_Other,
                    NameNode = er.Name_Other,
                    FillColor = "#FFCC00",
                    ShapeType = "rectangle",
                    TextColor = "#000000",
                    XmlTemplate = ERModel.Config.LocalData.Object_Graphml___ER_Node,
                    VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_ID.Name,
                            Value = er.ID_Other
                        },
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_NAME_NODE.Name,
                            Value = er.Name_Other
                        }
                    }
                }));

                entityNodes.AddRange(modelResult.Result.EntityRelations.Select(er => new NodeItem
                {
                    IdNode = er.IdEntityRight,
                    NameNode = er.NameEntityRight,
                    FillColor = "#FFCC00",
                    ShapeType = "rectangle",
                    TextColor = "#000000",
                    XmlTemplate = ERModel.Config.LocalData.Object_Graphml___ER_Node,
                    VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_ID.Name,
                            Value = er.IdEntityRight
                        },
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_NAME_NODE.Name,
                            Value = er.NameEntityRight
                        }
                    }
                }));

                entityNodes.AddRange(modelResult.Result.EntityRelations.Select(er => new NodeItem
                {
                    IdNode = er.IdEntityRelation,
                    NameNode = er.NameEntityRelation,
                    FillColor = "#FFCC00",
                    ShapeType = "diamond",
                    TextColor = "#000000",
                    XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_RelationType,
                    VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_ID.Name,
                            Value = er.IdEntityRelation
                        },
                        new VariableValueMap
                        {
                            Variable = Config.LocalData.Object_NAME_NODE.Name,
                            Value = er.NameEntityRelation
                        }
                    }
                }));

                if (exportType == ExportType.All || 
                    exportType == ExportType.EntitiesWithIdentAtt ||
                    exportType == ExportType.EntitiesWithIdentDerivedAtt)
                {
                    entityNodes.AddRange(modelResult.Result.EntitiesToAttributesIdentify.Select(attId => new NodeItem
                    {
                        IdNode = attId.ID_Other,
                        NameNode = attId.Name_Other,
                        FillColor = "#FFCC00",
                        ShapeType = "ellipse",
                        TextColor = "#000000",
                        XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Attribute,
                        VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_UNDERLINE.Name,
                            Value = "true"
                        },
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_BORDER_TYPE.Name,
                            Value = "line"
                        },
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_ATTRIBUTE_NAME.Name,
                            Value = attId.Name_Other
                        }
                    }
                    }));
                }

                var entitiesWithoutForeignKeys = (from attribute in modelResult.Result.EntitiesToAttributesDescribe
                                                  join foreignAttrib in modelResult.Result.EntityRelations on attribute.ID_Other equals foreignAttrib.IdAttributeConnected into foreignAttributes
                                                  from foreignAttrib in foreignAttributes.DefaultIfEmpty()
                                                  where foreignAttrib == null
                                                  select attribute).ToList();

                if (exportType == ExportType.All)
                {
                    entityNodes.AddRange(entitiesWithoutForeignKeys.Select(attId => new NodeItem
                    {
                        IdNode = attId.ID_Other,
                        NameNode = attId.Name_Other,
                        FillColor = "#FFCC00",
                        ShapeType = "ellipse",
                        TextColor = "#000000",
                        XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Attribute,
                        VariableValueMapList = new List<VariableValueMap>
                        {
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_UNDERLINE.Name,
                                Value = "false"
                            },
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_BORDER_TYPE.Name,
                                Value = "line"
                            },
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_ATTRIBUTE_NAME.Name,
                                Value = attId.Name_Other
                            }
                        }
                    }));

                    entityNodes.AddRange(modelResult.Result.EntityRelations.SelectMany(entityRel => entityRel.RelAttributeDescribe).Select(relAttDesc => new NodeItem
                    {
                        IdNode = relAttDesc.ID_Other,
                        NameNode = relAttDesc.Name_Other,
                        FillColor = "#FFCC00",
                        ShapeType = "ellipse",
                        TextColor = "#000000",
                        XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Attribute,
                        VariableValueMapList = new List<VariableValueMap>
                        {
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_UNDERLINE.Name,
                                Value = "false"
                            },
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_BORDER_TYPE.Name,
                                Value = "line"
                            },
                            new VariableValueMap
                            {
                                Variable = ERModel.Config.LocalData.Object_ATTRIBUTE_NAME.Name,
                                Value = relAttDesc.Name_Other
                            }
                        }
                    }));
                }

                if (exportType == ExportType.All ||
                    exportType == ExportType.EntitiesWithDerivedAtt ||
                    exportType == ExportType.EntitiesWithIdentDerivedAtt)
                {
                    entityNodes.AddRange(modelResult.Result.EntitiesToAttributesDerive.Select(attId => new NodeItem
                    {
                        IdNode = attId.ID_Other,
                        NameNode = attId.Name_Other,
                        FillColor = "#FFCC00",
                        ShapeType = "ellipse",
                        TextColor = "#000000",
                        XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Attribute,
                        VariableValueMapList = new List<VariableValueMap>
                    {
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_UNDERLINE.Name,
                            Value = "false"
                        },
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_BORDER_TYPE.Name,
                            Value = "dashed"
                        },
                        new VariableValueMap
                        {
                            Variable = ERModel.Config.LocalData.Object_ATTRIBUTE_NAME.Name,
                            Value = attId.Name_Other
                        }
                    }
                    }));
                }
                

                var entityEdges = (from entityRelation in modelResult.Result.EntityRelations
                                   join leftNode in entityNodes on entityRelation.IdEntityLeft equals leftNode.IdNode
                                   join relationNode in entityNodes on entityRelation.IdEntityRelation equals relationNode.IdNode
                                   select new EdgeItem
                                   {
                                       NodeItem1 = leftNode,
                                       NodeItem2 = relationNode,
                                       XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge,
                                       VariableValueMapList = new List<VariableValueMap>
                                       {
                                           new VariableValueMap
                                           {
                                               Variable = ERModel.Config.LocalData.Object_EDGE_NAME.Name,
                                               Value = entityRelation.NameCardinalityLeft
                                           }
                                       }
                                   }).ToList();

                entityEdges.AddRange(from entityRelation in modelResult.Result.EntityRelations
                                     join relationNode in entityNodes on entityRelation.IdEntityRelation equals relationNode.IdNode
                                     join rightNode in entityNodes on entityRelation.IdEntityRight equals rightNode.IdNode
                                     select new EdgeItem
                                     {
                                         NodeItem1 = relationNode,
                                         NodeItem2 = rightNode,
                                         XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge,
                                         VariableValueMapList = new List<VariableValueMap>
                                       {
                                           new VariableValueMap
                                           {
                                               Variable = ERModel.Config.LocalData.Object_EDGE_NAME.Name,
                                               Value = entityRelation.NameCardinalityRight
                                           }
                                       }
                                     });

                if (exportType == ExportType.All ||
                    exportType == ExportType.EntitiesWithIdentAtt ||
                    exportType == ExportType.EntitiesWithIdentDerivedAtt)
                {
                    entityEdges.AddRange(from attributeIdent in modelResult.Result.EntitiesToAttributesIdentify
                                         join attributeNode in entityNodes on attributeIdent.ID_Other equals attributeNode.IdNode
                                         join entityNode in entityNodes on attributeIdent.ID_Object equals entityNode.IdNode
                                         select new EdgeItem
                                         {
                                             NodeItem1 = entityNode,
                                             NodeItem2 = attributeNode,
                                             XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge_Attribute
                                         });

                    
                }

                if (exportType == ExportType.All)
                {
                    entityEdges.AddRange(from attributeDescribe in entitiesWithoutForeignKeys
                                         join attributeNode in entityNodes on attributeDescribe.ID_Other equals attributeNode.IdNode
                                         join entityNode in entityNodes on attributeDescribe.ID_Object equals entityNode.IdNode
                                         select new EdgeItem
                                         {
                                             NodeItem1 = entityNode,
                                             NodeItem2 = attributeNode,
                                             XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge_Attribute
                                         });

                    entityEdges.AddRange(from entityRelation in modelResult.Result.EntityRelations.SelectMany(er => er.RelAttributeDescribe).ToList()
                                         join entityRelationNode in entityNodes on entityRelation.ID_Object equals entityRelationNode.IdNode
                                         join entityAttribute in entityNodes on entityRelation.ID_Other equals entityAttribute.IdNode
                                         select new EdgeItem
                                         {
                                             NodeItem1 = entityRelationNode,
                                             NodeItem2 = entityAttribute,
                                             XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge_Attribute
                                         });

                }

                if (exportType == ExportType.All ||
                    exportType == ExportType.EntitiesWithDerivedAtt ||
                    exportType == ExportType.EntitiesWithIdentDerivedAtt)
                {
                    entityEdges.AddRange(from attributeDerive in modelResult.Result.EntitiesToAttributesDerive
                                         join attributeNode in entityNodes on attributeDerive.ID_Other equals attributeNode.IdNode
                                         join entityNode in entityNodes on attributeDerive.ID_Object equals entityNode.IdNode
                                         select new EdgeItem
                                         {
                                             NodeItem1 = entityNode,
                                             NodeItem2 = attributeNode,
                                             XmlTemplate = ERModel.Config.LocalData.Object_GraphML___ER_Edge_Attribute
                                         });
                }

                using (TextWriter textWriter = new StreamWriter(modelResult.Result.ConfigItemToExportPath.Name_Other))
                {
                    request.MessageOutput?.OutputInfo("Write Container init...");
                    var writePreContainerResult = factory.WritePreContainer((text) =>
                    {
                        textWriter.WriteLine(text);
                    }, ERModel.Config.LocalData.Object_GraphML___ER_Container);

                    result.ResultState = writePreContainerResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo("Written Container init.");

                    foreach (var entityNode in entityNodes)
                    {
                        factory.WriteNodeXML(entityNode, (text) =>
                        {
                            textWriter.WriteLine(text);
                        }, "");
                    }

                    foreach (var entityEdge in entityEdges)
                    {
                        factory.WriteEdgeXML(entityEdge, (text) =>
                        {
                            textWriter.WriteLine(text);
                        }, "");
                    }

                    request.MessageOutput?.OutputInfo("Write Container finish...");
                    var writeContainerRestResult = factory.WritePostContainer(new List<clsOntologyItem>(), (text) =>
                    {
                        textWriter.WriteLine(text);
                    }, ERModel.Config.LocalData.Object_GraphML___ER_Container);

                    result.ResultState = writeContainerRestResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo("Written Container finish.");
                }

                return result;
            });

            return taskResult;
        }

        public GraphMLConnector(Globals globals) : base(globals)
        {
        }
    }
}
