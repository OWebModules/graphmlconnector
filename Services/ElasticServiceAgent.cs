﻿using GraphMLConnector.Models;
using GraphMLConnector.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphMLConnector.Services
{
    public class ElasticServiceAgent : ElasticBaseAgent
    {
        public async Task<ResultItem<ExportTypedTagsToGraphMLModel>> GetExportTypedTagsToGraphMLModel(ExportTypedTagsToGraphMLRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExportTypedTagsToGraphMLModel>>(() =>
           {
               var result = new ResultItem<ExportTypedTagsToGraphMLModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ExportTypedTagsToGraphMLModel()
               };

               result.ResultState = ValidationController.ValidateExportTypedTagsToGraphMLRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = TypedTags.Config.LocalData.Class_ExportTypedTags_To_GraphML.GUID
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-Config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateExportTypedTagsToGraphMLModel(result.Result, globals, nameof(result.Result.RootConfig));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = TypedTags.Config.LocalData.ClassRel_ExportTypedTags_To_GraphML_contains_ExportTypedTags_To_GraphML.ID_RelationType,
                       ID_Parent_Other = TypedTags.Config.LocalData.ClassRel_ExportTypedTags_To_GraphML_contains_ExportTypedTags_To_GraphML.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the sub-configs!";

                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }


               var searchPaths = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = TypedTags.Config.LocalData.ClassRel_ExportTypedTags_To_GraphML_export_to_Path.ID_RelationType,
                   ID_Parent_Other = TypedTags.Config.LocalData.ClassRel_ExportTypedTags_To_GraphML_export_to_Path.ID_Class_Right
               }).ToList();

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the paths!";
                   return result;
               }

               result.Result.ConfigsToPaths = dbReaderPaths.ObjectRels;

               result.ResultState = ValidationController.ValidateExportTypedTagsToGraphMLModel(result.Result, globals, nameof(result.Result.ConfigsToPaths));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchTaggingSources = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = TypedTags.Config.LocalData.ClassRel_ExportTypedTags_To_GraphML_belonging_Source.ID_RelationType
               }).ToList();

               var dbReaderTaggingSources = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTaggingSources.GetDataObjectRel(searchTaggingSources);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the tagging-sources!";
                   return result;
               }

               result.Result.ConfigsToTaggingSources = dbReaderTaggingSources.ObjectRels;

               result.ResultState = ValidationController.ValidateExportTypedTagsToGraphMLModel(result.Result, globals, nameof(result.Result.ConfigsToTaggingSources));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetClasses(List<clsOntologyItem> searchList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };


                var dbReaderClasses = new OntologyModDBConnector(globals);

                if (searchList.Any())
                {
                    result.ResultState = dbReaderClasses.GetDataClasses(searchList);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the classes!";
                        return result;
                    }
                }

                result.Result = dbReaderClasses.Classes1;


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportERModelModel>> GetExportERModelModel(ExportERModelRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<ExportERModelModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ExportERModelModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderConfig, globals, nameof(ExportERModelModel.ConfigItem));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchERModels = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.ConfigItem.GUID,
                        ID_RelationType = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_uses_ER_Model.ID_RelationType,
                        ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_uses_ER_Model.ID_Class_Right
                    }
                };

                var dbReaderERModels = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderERModels.GetDataObjectRel(searchERModels);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the ER-Models!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderERModels, globals, nameof(ExportERModelModel.ConfigItemToERModels));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPath = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.ConfigItem.GUID,
                        ID_RelationType = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_export_to_Path.ID_RelationType,
                        ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_export_to_Path.ID_Class_Right
                    }
                };

                var dbReaderSearchPath = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSearchPath.GetDataObjectRel(searchPath);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Path to export!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderSearchPath, globals, nameof(ExportERModelModel.ConfigItemToExportPath));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigToExportType = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.ConfigItem.GUID,
                        ID_RelationType = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_is_of_Type_Export_Type__ER_Model_.ID_RelationType,
                        ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Export_ER_Model_to_GraphML_is_of_Type_Export_Type__ER_Model_.ID_Class_Right
                    }
                };

                var dbReaderConfigToExportType = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigToExportType.GetDataObjectRel(searchConfigToExportType);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Export-Type!";
                    return result;
                }
                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderConfigToExportType, globals, nameof(ExportERModelModel.ConfigItemToExportType));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchErToEntities = result.Result.ConfigItemToERModels.Select(cToEr => new clsObjectRel
                {
                    ID_Object = cToEr.ID_Other,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_ER_Model_contains_Entity.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_ER_Model_contains_Entity.ID_Class_Right
                }).ToList();

                var dbReaderErToEntities = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderErToEntities.GetDataObjectRel(searchErToEntities);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the ER-Models to Entities relations!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderErToEntities, globals, nameof(ExportERModelModel.ERModelToEntities));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchErToEntityRelations = result.Result.ConfigItemToERModels.Select(cToEr => new clsObjectRel
                {
                    ID_Object = cToEr.ID_Other,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_ER_Model_contains_Entity_Relation.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_ER_Model_contains_Entity_Relation.ID_Class_Right
                }).ToList();

                var dbReaderErToEntityRelations = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderErToEntityRelations.GetDataObjectRel(searchErToEntityRelations);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the ER-Models!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderErToEntityRelations, globals, nameof(ExportERModelModel.ERModelToEntityRelations));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchEntityRelationsRels = result.Result.ERModelToEntityRelations.Select(erModelToEntityRel => new clsObjectRel
                {
                    ID_Object = erModelToEntityRel.ID_Other,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_Relation_connected_by_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_Relation_connected_by_Attribute__Entity_.ID_Class_Right
                }).ToList();

                searchEntityRelationsRels.AddRange(result.Result.ERModelToEntityRelations.Select(erModelToEntityRel => new clsObjectRel
                {
                    ID_Object = erModelToEntityRel.ID_Other,
                    ID_Parent_Other = ERModel.Config.LocalData.Class_Cardinality.GUID
                }));

                searchEntityRelationsRels.AddRange(result.Result.ERModelToEntityRelations.Select(erModelToEntityRel => new clsObjectRel
                {
                    ID_Object = erModelToEntityRel.ID_Other,
                    ID_Parent_Other = ERModel.Config.LocalData.Class_Entity.GUID
                }));

                searchEntityRelationsRels.AddRange(result.Result.ERModelToEntityRelations.Select(erModelToEntityRel => new clsObjectRel
                {
                    ID_Object = erModelToEntityRel.ID_Other,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_Relation_is_of_Type_Modelling_Pattern.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_Relation_is_of_Type_Modelling_Pattern.ID_Class_Right
                }));

                searchEntityRelationsRels.AddRange(result.Result.ERModelToEntityRelations.Select(erModelToEntityRel => new clsObjectRel
                {
                    ID_Object = erModelToEntityRel.ID_Other,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_Relation_describes_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_Relation_describes_Attribute__Entity_.ID_Class_Right
                }));

                var dbReaderEntityRelationsRel = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderEntityRelationsRel.GetDataObjectRel(searchEntityRelationsRels);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Relations of Entity-Relations (Connection-Attribute, Cardinalities, Entities, Modelling-Pattern)!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderEntityRelationsRel, globals, nameof(ExportERModelModel.EntityRelationsToAttributes));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var entityIds = result.Result.EntityRelations.GroupBy(entityRel => entityRel.IdEntityLeft).Select(entityId => entityId.Key).ToList();
                entityIds.AddRange(from entityId in result.Result.EntityRelations.GroupBy(entityRel => entityRel.IdEntityRight).Select(entityId => entityId.Key)
                                   join insertedId in entityIds on entityId equals insertedId into insertedIds
                                   from insertedId in insertedIds.DefaultIfEmpty()
                                   where insertedId == null
                                   select entityId);

                entityIds.AddRange(from entityId in result.Result.ERModelToEntities.GroupBy(entityRel => entityRel.ID_Other).Select(entityId => entityId.Key)
                                   join insertedId in entityIds on entityId equals insertedId into insertedIds
                                   from insertedId in insertedIds.DefaultIfEmpty()
                                   where insertedId == null
                                   select entityId);

                var searchEntitiesToAttributes = entityIds.Select(entityId => new clsObjectRel
                {
                    ID_Object = entityId,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_identified_by_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_identified_by_Attribute__Entity_.ID_Class_Right
                }).ToList();

                searchEntitiesToAttributes.AddRange(entityIds.Select(entityId => new clsObjectRel
                {
                    ID_Object = entityId,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_contains_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_contains_Attribute__Entity_.ID_Class_Right
                }));

                searchEntitiesToAttributes.AddRange(entityIds.Select(entityId => new clsObjectRel
                {
                    ID_Object = entityId,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Entity_Derived_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Entity_Derived_Attribute__Entity_.ID_Class_Right
                }));

                var dbReaderEntitiesToAttributes = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderEntitiesToAttributes.GetDataObjectRel(searchEntitiesToAttributes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Attributes of Entities!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderEntitiesToAttributes, globals, nameof(ExportERModelModel.EntitiesToAttributesIdentify));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchAttributesIsAttributes = result.Result.EntityAttributeIds.Select(attId => new clsObjectRel
                {
                    ID_Object = attId,
                    ID_RelationType = ERModel.Config.LocalData.ClassRel_Attribute__Entity__is_Attribute__Entity_.ID_RelationType,
                    ID_Parent_Other = ERModel.Config.LocalData.ClassRel_Attribute__Entity__is_Attribute__Entity_.ID_Class_Right
                }).ToList();

                var dbReaderAttributesIsAttributes = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderAttributesIsAttributes.GetDataObjectRel(searchAttributesIsAttributes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the equal Attributes!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportERModelModel(result.Result, dbReaderAttributesIsAttributes, globals, nameof(ExportERModelModel.EntityAttributesIsEntityAttributes));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticServiceAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
