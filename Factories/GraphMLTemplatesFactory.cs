﻿using GraphMLConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace GraphMLConnector.Factories
{
    public class GraphMLTemplatesFactory
    {
        protected Globals globals;

        protected virtual ResultItem<string> PrepareNodeXML(NodeItem node, string defaultXml = null)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };

            var nodeXMLResult = GetNodeXML(node, defaultXml);
            result.ResultState = nodeXMLResult.ResultState;

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = nodeXMLResult.Result;
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ID.Name}@", node.IdNode);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_NAME_NODE.Name}@", node.NameNode);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_COLOR_FILL.Name}@", node.FillColor);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_COLOR_TEXT.Name}@", node.TextColor);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_SOURCE_ID.Name}@", node.SVGSourceId.ToString());
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_SHAPE_TYPE.Name}@", node.ShapeType);

            node.VariableValueMapList.ForEach(varValueMap =>
            {
                result.Result = result.Result.Replace($"@{varValueMap.Variable}@", HttpUtility.HtmlEncode(varValueMap.Value));
            });

            var getSubNodeXMLResult = GetSubNodeText(node, result.Result);
            result.ResultState = getSubNodeXMLResult.ResultState;

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = getSubNodeXMLResult.Result;

            return result;
        }

        public clsOntologyItem WriteEdgeXML(EdgeItem edge, Action<string> callbackWriteText, string defaultXml = null)
        {
            var result = globals.LState_Success.Clone();
            var resultPrepare = PrepareEdgeXML(edge, defaultXml);
            result = resultPrepare.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            try
            {
                callbackWriteText(resultPrepare.Result);
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        protected ResultItem<string> PrepareEdgeXML(EdgeItem edge, string defaultXml = null)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };

            var edgeXMLResult = GetEdgeXML(edge, defaultXml);
            result.ResultState = edgeXMLResult.ResultState;

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = edgeXMLResult.Result;
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ID.Name}@", edge.NodeItem1.IdNode + edge.NodeItem2.IdNode + (edge.RelationType?.GUID ?? ""));
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ID_LEFT.Name}@", edge.NodeItem1.IdNode);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ID_RIGHT.Name}@", edge.NodeItem2.IdNode);
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_NAME_RELATIONTYPE.Name}@", edge.RelationType?.Name ?? "");
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_SOURCE_ID.Name}@", edge.SVGSourceId.ToString());
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ARROW_SOURCE.Name}@", edge.ArrowTypeSource.ToString());
            result.Result = result.Result.Replace($"@{Config.LocalData.Object_ARROW_TARGET.Name}@", edge.ArrowTypeTarget.ToString());

            edge.VariableValueMapList.ForEach(varValueMap =>
            {
                result.Result = result.Result.Replace($"@{varValueMap.Variable}@", HttpUtility.HtmlEncode(varValueMap.Value));
            });

            return result;
        }

        public clsOntologyItem WriteNodeXML(NodeItem node, Action<string> callbackWriteText, string defaultXml = null)
        {
            var result = globals.LState_Success.Clone();
            var resultPrepare = PrepareNodeXML(node, defaultXml);
            result = resultPrepare.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            try
            {
                callbackWriteText(resultPrepare.Result);
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        protected ResultItem<string> GetSubNodeText(NodeItem parentItem, string nodeText)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };

            result.Result = nodeText;

            foreach (var snl in parentItem.SubNodeList)
            {
                var textSubNodes = "";
                foreach (var sn in snl.SubNodes)
                {
                    var textSubNode = "";
                    if (sn.XmlTemplate != null)
                    {
                        var resultGetXMLContent = GetXMLContent(sn.XmlTemplate);
                        result.ResultState = resultGetXMLContent.ResultState;
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        textSubNode = resultGetXMLContent.Result;
                    }
                    if (!sn.VariableValueMapList.Any())
                    {
                        if (!string.IsNullOrEmpty(textSubNode))
                        {
                            textSubNode += "\n";
                        }
                        textSubNode += sn.NameNode;
                    }
                    else
                    {
                        sn.VariableValueMapList.ForEach(vvm =>
                        {
                            textSubNode = textSubNode.Replace("@" + vvm.Value + "@", HttpUtility.HtmlEncode(vvm.Value));
                        });
                    }
                    if (!string.IsNullOrEmpty(textSubNodes))
                    {
                        textSubNodes += "\n";
                    }
                    textSubNodes += textSubNode;
                    var getSubNodeTextResult = GetSubNodeText(sn, textSubNodes);
                    result.ResultState = getSubNodeTextResult.ResultState;

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    textSubNodes = getSubNodeTextResult.Result;
                }

                result.Result = result.Result.Replace("@" + snl.Variable + "@", textSubNodes);
            }
            return result;
        }

        public ResultItem<string> GetXMLContent(clsOntologyItem oItemXmlItem)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };


            XmlDocument xmlDoc = new XmlDocument();
            XmlNodeList xmlData;

            var searchXMLAtt = new List<clsObjectAtt>{new clsObjectAtt
            {
                ID_Object = oItemXmlItem.GUID,
                ID_AttributeType = Config.LocalData.AttributeType_XML_Text.GUID
            } };

            var dbReaderXMLAtt = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderXMLAtt.GetDataObjectAtt(searchXMLAtt);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the XML Text!";
                return result;
            }

            if (!dbReaderXMLAtt.ObjAtts.Any())
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "No XML Template found!";
                return result;
            }

            var xmlText = dbReaderXMLAtt.ObjAtts.First().Val_String;

            try
            {
                xmlDoc.LoadXml(xmlText);
                xmlData = xmlDoc.GetElementsByTagName("data");
                if (xmlData.Count > 0)
                {
                    result.Result = xmlData[0].InnerText;
                }
                else
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The XML-Template does not have the correct structure! No data-section found!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = ex.Message;
                return result;
            }

            return result;
        }

        public ResultItem<string> GetNodeXML(NodeItem node, string defaultXml)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };

            if (node.XmlTemplate != null)
            {
                var xmlContentResult = GetXMLContent(node.XmlTemplate);
                result.ResultState = xmlContentResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = xmlContentResult.Result;
            }
            else
            {
                result.Result = defaultXml;
            }

            return result;
        }

        public ResultItem<string> GetEdgeXML(EdgeItem edge, string defaultXml)
        {
            var result = new ResultItem<string>
            {
                ResultState = globals.LState_Success.Clone()
            };

            if (edge.XmlTemplate != null)
            {
                var xmlContentResult = GetXMLContent(edge.XmlTemplate);
                result.ResultState = xmlContentResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = xmlContentResult.Result;
            }
            else
            {
                result.Result = defaultXml;
            }

            return result;
        }

        public virtual clsOntologyItem WritePreContainer(Action<string> callbackWriteText, clsOntologyItem containerXML)
        {
            var result = globals.LState_Success.Clone();

            var getContentResult = GetXMLContent(containerXML);
            result = getContentResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            try
            {
                callbackWriteText(getContentResult.Result.Substring(0, getContentResult.Result.IndexOf($"@{Config.LocalData.Object_NODE_LIST.Name}@") - 1));
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        public virtual clsOntologyItem WritePostContainer(List<clsOntologyItem> svgSources, Action<string> callbackWriteText, clsOntologyItem containerXML)
        {
            var result = globals.LState_Success.Clone();

            var getContentResult = GetXMLContent(containerXML);
            result = getContentResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            try
            {
                var containerRest = getContentResult.Result.Substring(getContentResult.Result.IndexOf($"@{Config.LocalData.Object_EDGE_LIST.Name}@") + $"@{Config.LocalData.Object_EDGE_LIST.Name}@".Length);
                if (containerRest.Contains($"@{ Config.LocalData.Object_RESOURCE_LIST.Name}@"))
                {
                    var toResources = containerRest.Substring(0, containerRest.IndexOf($"@{ Config.LocalData.Object_RESOURCE_LIST.Name}@") - 1);

                    callbackWriteText(toResources);

                    for (int i = 0; i < svgSources.Count; i++)
                    {
                        var getXMLContentResult = GetXMLContent(svgSources[i]);
                        result = getXMLContentResult.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var resourceString = getXMLContentResult.Result.Replace($"@{Config.LocalData.Object_SOURCE_ID.Name}@", (i + 1).ToString());
                        callbackWriteText(resourceString);
                    }
                    containerRest = containerRest.Substring(toResources.Length + ($"@{Config.LocalData.Object_RESOURCE_LIST.Name}@").Length + 1);
                }
                
                callbackWriteText(containerRest);
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

        }

        public virtual clsOntologyItem Initialize()
        {
            return globals.LState_Success.Clone();
        }

        public GraphMLTemplatesFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
