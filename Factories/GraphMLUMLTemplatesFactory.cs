﻿using GraphMLConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace GraphMLConnector.Factories
{
    public class GraphMLUMLTemplatesFactory : GraphMLTemplatesFactory
    {
        public string UMLContainer { get; set; }
        public string UMLEdge { get; set; }
        public string UMLClassNode { get; set; }
        public string UMLGroup { get; set; }

        public string RDF { get; set; }

        public override clsOntologyItem WritePreContainer(Action<string> callbackWriteText, clsOntologyItem containerXML)
        {
            return base.WritePreContainer(callbackWriteText, containerXML);
        }

        public override clsOntologyItem WritePostContainer(List<clsOntologyItem> svgSources, Action<string> callbackWriteText, clsOntologyItem containerXML)
        {
            return base.WritePostContainer(svgSources, callbackWriteText, containerXML);
        }

        private clsOntologyItem WritePreGroup(string groupId,
                                      string graphId,
                                      string groupName,
                                      Action<string> callbackWriteText)
        {
            var result = globals.LState_Success.Clone();
            var groupString = UMLGroup.Replace($"@{Config.LocalData.Object_ID.Name}@", groupId).
                                       Replace($"@{Config.LocalData.Object_GRAPH_ID.Name}@", graphId).
                                       Replace($"@{Config.LocalData.Object_NAME_NODE.Name}@", groupName);

            try
            {
                callbackWriteText(groupString.Substring(0, groupString.IndexOf($"@{Config.LocalData.Object_GRAPH_ITEMS.Name}@") - 1));
                return result;
            }
            catch (Exception ex)
            {

                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        public clsOntologyItem WritePostGroup(Action<string> callbackWriteText)
        {
            var result = globals.LState_Success.Clone();

            try
            {
                callbackWriteText(UMLGroup.Substring(UMLGroup.IndexOf($"@{Config.LocalData.Object_GRAPH_ITEMS.Name}@") + $"@{Config.LocalData.Object_GRAPH_ITEMS.Name}@".Length));
                return result;
            }
            catch (Exception ex)
            {

                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        public clsOntologyItem WriteNodeXML(NodeItem node, Action<string> callbackWriteText)
        {
            return base.WriteNodeXML(node, callbackWriteText, UMLClassNode);
        }

        private ResultItem<string> PrepareNodeXML(NodeItem node)
        {
            return base.PrepareNodeXML(node, UMLClassNode);
        }

        public clsOntologyItem WriteEdgeXML(EdgeItem edge, Action<string> callbackWriteText)
        {
            return base.WriteEdgeXML(edge, callbackWriteText, UMLEdge);
        }

        private ResultItem<string> PrepareEdgeXML(EdgeItem edge)
        {
            return base.PrepareEdgeXML(edge, UMLEdge);
        }

        public ResultItem<NodeEdgePackage> CreateGroup(GroupItem groupItem, Action<string> callbackWriteText, IMessageOutput messageOutput)
        {
            var result = new ResultItem<NodeEdgePackage>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new NodeEdgePackage()
            };

            result.Result.NodeItems = groupItem.Nodes;
            result.Result.EdgeItems = groupItem.Edges;

            result.ResultState = WritePreGroup(groupItem.GroupId, groupItem.GraphId, groupItem.GroupName, callbackWriteText);
            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            groupItem.Nodes.ForEach(node =>
            {
                node.IdNode = $"{groupItem.GroupId}{node.IdNode}";
            });

            messageOutput?.OutputInfo($"Write {groupItem.Nodes.Count} nodes of group {groupItem.GroupName}...");

            var i = 0;
            foreach (var node in groupItem.Nodes)
            {
                var getNodeXMLResult = PrepareNodeXML(node);

                result.ResultState = getNodeXMLResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var nodeXML = getNodeXMLResult.Result;

                

                try
                {
                    callbackWriteText(nodeXML);
                }
                catch (Exception ex)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;   
                }
                if (i > 0 && i % 100 == 0)
                {
                    messageOutput?.OutputInfo($"Written {i} nodes.");
                }
                i++;
            }

            messageOutput?.OutputInfo($"Written {groupItem.Nodes.Count} nodes.");

            messageOutput?.OutputInfo($"Write {groupItem.Edges.Count} edges...");

            i = 0;
            foreach (var edge in groupItem.Edges)
            {
                var edgeXMLResult = PrepareEdgeXML(edge);
                result.ResultState = edgeXMLResult.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                try
                {
                    callbackWriteText(edgeXMLResult.Result);
                }
                catch (Exception ex)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                if (i > 0 && i % 100 == 0)
                {
                    messageOutput?.OutputInfo($"Written {i} edges.");
                }
                i++;
            }

            messageOutput?.OutputInfo($"Written {groupItem.Edges.Count} edges.");

            foreach (var subGroupItem in groupItem.GroupItems)
            {
                var createSubGroupResult = CreateGroup(subGroupItem, callbackWriteText, messageOutput);
                result.ResultState = createSubGroupResult.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result.NodeItems.AddRange(createSubGroupResult.Result.NodeItems);
                result.Result.EdgeItems.AddRange(createSubGroupResult.Result.EdgeItems);
            }

            result.ResultState = WritePostGroup(callbackWriteText);

            return result;
        }

        public GraphMLUMLTemplatesFactory(Globals globals) : base(globals)
        {
        }

        public override clsOntologyItem Initialize()
        {
            var result = globals.LState_Success.Clone();
            var getXMLTextResult = GetXMLContent(Config.LocalData.Object_Graphml___Container);
            result = getXMLTextResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            UMLContainer = getXMLTextResult.Result;

            getXMLTextResult = GetXMLContent(Config.LocalData.Object_Graphml___UML_Class_Node);
            result = getXMLTextResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            UMLClassNode = getXMLTextResult.Result;

            getXMLTextResult = GetXMLContent(Config.LocalData.Object_Graphml___UML_Edge);
            result = getXMLTextResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            UMLEdge = getXMLTextResult.Result;

            getXMLTextResult = GetXMLContent(Config.LocalData.Object_Graphml___Group);
            result = getXMLTextResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            UMLGroup = getXMLTextResult.Result;

            getXMLTextResult = GetXMLContent(Config.LocalData.Object_Graphml___RDF);
            result = getXMLTextResult.ResultState;

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            RDF = getXMLTextResult.Result;

            return result;
        }


    }
}
