﻿using GraphMLConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace GraphMLConnector.Factories
{
    public class GraphMLTagsFactory
    {

        public Globals globals;

        public async Task<ResultItem<NodeEdgePackage>> CreateNodeEdgePackages(clsOntologyItem taggingSource, List<Reference> references)
        {
            var taskResult = await Task.Run<ResultItem<NodeEdgePackage>>(() =>
            {
                var result = new ResultItem<NodeEdgePackage>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new NodeEdgePackage()
                };

                var nodeItem = new NodeItem
                {
                    IdNode = taggingSource.GUID,
                    NameNode = taggingSource.Name,
                    ShapeType = Config.LocalData.Object_rectangle.Name,
                    FillColor = "#ffffff",
                    TextColor = "#000000"
                };

                

                return result;
            });

            return taskResult;
        }

        public GraphMLTagsFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
